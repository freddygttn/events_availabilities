import moment from 'moment'
import knex from 'knexClient'
import AppError from './model/Error'
import getEventSlots from './getEventSlots'
import eventsReducer from './events-reducer'

export default async function getAvailabilities(date) {
  // Check for input
  if (!(date instanceof Date)) {
    return Promise.reject(AppError.NotADateParameter)
  }
  if (!moment(date).isValid()) {
    return Promise.reject(AppError.InvalidDateParameter)
  }

  // Query
  const queryResult = await knex('events')
    // The event is between requested date and requested date + 6 days
    .whereBetween('starts_at', [
      moment(date).format('x'),
      moment(date).add(6, 'd').format('x'),
    ])
    // Or the event starts before requested date but is weekly recurring
    .orWhere(function() {
      this.where('starts_at', '<', moment(date).add(6, 'd').format('x'))
        .andWhere('weekly_recurring', true)
    })
    // Or the event is overlapping
    .orWhere(function() {
      this.where('starts_at', '<=', moment(date).format('x'))
        .andWhere('ends_at', '>=', moment(date).format('x'))
    })

  // Initialize availabilities to return
  const initialAvailabilities = []
  for (let i = 0; i < 7; ++i) {
    initialAvailabilities.push({
      date: moment(date).add(i, 'd').toDate(),
      slots: []
    })
  }

  // By looking at the database model, result.kind, result.starts_at
  // and result.ends_at will always be there with a correct type
  return queryResult
    // Transform the events to keep only the kind and their slots
    .map((result) => ({
      kind: result.kind,
      slots: getEventSlots(date, result)
    }))
    // Reduce each one into our availabilities
    .reduce(eventsReducer, initialAvailabilities)
}
