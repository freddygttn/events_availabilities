export default {

  NotADateParameter: {
    code: 0,
    error: 'Invalid parameter',
  },

  InvalidDateParameter: {
    code: 1,
    error: 'Invalid parameter',
  },

}