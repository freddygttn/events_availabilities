import removeSlots from './removeSlots'

describe('removeSlots', () => {
  it('should remove slots from current state', () => {
    const initialState = [
      {slots: ['9:30', '10:00']},
      {slots: ['9:30']},
      {slots: ['11:30']},
    ]
    const slots = [
      {index: 0, slot: '9:30'},
      {index: 1, slot: '9:30'},
      {index: 0, slot: '10:00'},
    ]
    const expectedState = [{slots: []}, {slots: []}, {slots: ['11:30']}]

    expect(removeSlots(initialState, slots)).toEqual(expectedState)
  })

  it('should be idempotent', () => {
    const initialState = [
      {slots: ['9:30', '10:00']},
      {slots: ['9:30']},
      {slots: ['11:30']},
    ]
    const slots = [
      {index: 0, slot: '9:30'},
      {index: 1, slot: '9:30'},
      {index: 0, slot: '10:00'},
    ]

    const expectedState = [{slots: []}, {slots: []}, {slots: ['11:30']}]
    const output = removeSlots(removeSlots(initialState, slots), slots)

    expect(output).toEqual(expectedState)
  })

  it('should throw if initial state is invalid', () => {
    const initialState = []
    const slots = [
      {index: 0, slot: '9:30'},
      {index: 1, slot: '9:30'},
      {index: 0, slot: '10:00'},
    ]

    expect(() => removeSlots(initialState, slots)).toThrow()
  })
})
