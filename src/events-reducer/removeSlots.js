/**
 * Removes slots from a given state.
 * @param {*} state State containing slots lists
 * @param {*} slots Slots described by their index and value
 */
export default function(state, slots) {
  const newSlots = state.slice()
  slots.forEach((s) => {
    const currIndex = newSlots[s.index].slots.indexOf(s.slot)
    if (currIndex !== -1) {
      newSlots[s.index].slots = newSlots[s.index].slots
        .slice(0, currIndex)
        .concat(newSlots[s.index].slots.slice(currIndex + 1, newSlots[s.index].slots.length))
    }
  })

  return newSlots
}
