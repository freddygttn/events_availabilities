import mergeSlots from './mergeSlots'

describe('mergeSlots', () => {
  it('should merge slots to current state', () => {
    const initialState = [{slots: []}, {slots: []}, {slots: ['11:30']}]
    const slots = [
      {index: 0, slot: '9:30'},
      {index: 1, slot: '9:30'},
      {index: 0, slot: '10:00'},
    ]
    const expectedState = [
      {slots: ['9:30', '10:00']},
      {slots: ['9:30']},
      {slots: ['11:30']}
    ]
    
    expect(mergeSlots(initialState, slots)).toEqual(expectedState)
  })

  it('should be idempotent', () => {
    const initialState = [{slots: []}, {slots: []}, {slots: ['11:30']}]
    const slots = [
      {index: 0, slot: '9:30'},
      {index: 1, slot: '9:30'},
      {index: 0, slot: '10:00'},
    ]
    const expectedState = [
      {slots: ['9:30', '10:00']},
      {slots: ['9:30']},
      {slots: ['11:30']}
    ]

    const output = mergeSlots(mergeSlots(initialState, slots), slots)
    
    expect(output).toEqual(expectedState)
  })

  it('should throw if initial state is invalid', () => {
    const initialState = []
    const slots = [
      {index: 2, slot: '11:30'},
    ]

    expect(() => mergeSlots(initialState, slots)).toThrow()
  })
})
