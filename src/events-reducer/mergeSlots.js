/**
 * Merges slots with a state.
 * @param {*} state State containing slots lists
 * @param {*} slots Slots described by their index and value
 */
export default function(state, slots) {
  const newSlots = state.slice()
  slots.forEach((s) => {
    if (newSlots[s.index].slots.indexOf(s.slot) === -1) {
      newSlots[s.index].slots = newSlots[s.index].slots.concat(s.slot)
    }
  });

  return newSlots
}
