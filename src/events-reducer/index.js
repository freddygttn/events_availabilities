import EventKind from '../model/EventKind';
import mergeSlots from './mergeSlots';
import removeSlots from './removeSlots';

export default function(state, event) {
  switch(event.kind) {

  case EventKind.Appointment:
    return removeSlots(state, event.slots)

  case EventKind.Opening:
    return mergeSlots(state, event.slots)

  default:
    return state
  }
}
