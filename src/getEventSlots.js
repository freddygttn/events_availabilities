import moment from 'moment'

/**
 * Creates all possible slots for a given event.
 * @param {*} date The starting date of our availabilities
 * @param {*} event The event
 * @param {*} step Minutes between each slot, defaults to 30
 */
export default function(date, event, step = 30) {
  const mDate = moment(date)
  const mStarts = moment(event.starts_at)
  const mStartPlus7 = moment(event.starts_at).add(7, 'd')
  const mEnds = moment(event.ends_at)

  // Restrict interval in case of very long appointments
  const mCurr = moment(event.starts_at)
  const mEndingDate = mEnds.isAfter(mStartPlus7) ? mStartPlus7 : mEnds
  const slots = []
  // Get the date weekday that will be used as our pivot to compute array index
  const pivotDay = 7 - mDate.weekday()
  while(mCurr.isBefore(mEndingDate)) {
    slots.push({
      index: (mCurr.weekday() + pivotDay) % 7,
      slot: mCurr.format('H:mm')
    })
    mCurr.add(step, 'm')
  }

  return slots
}
