import knex from 'knexClient'
import getAvailabilities from './getAvailabilities'
import AppError from './model/Error'

describe('getAvailabilities', () => {
  beforeEach(() => knex('events').truncate())

  describe('invalid parameter', () => {
    it('should reject if date is badly formatted', async () => {
      await expect(getAvailabilities(new Date('2014-08-42'))).rejects.toEqual(
        AppError.InvalidDateParameter
      )
    })

    it('should reject if input is not a date', async () => {
      await expect(getAvailabilities(9000)).rejects.toEqual(
        AppError.NotADateParameter
      )
    })
  })

  describe('empty case', () => {
    
    it('should fetch empty availabilities correctly', async () => {
      const availabilities = await getAvailabilities(new Date('2014-08-10'))
      expect(availabilities.length).toBe(7)

      expect(availabilities[0].slots).toEqual([])
      expect(availabilities[1].slots).toEqual([])
      expect(availabilities[2].slots).toEqual([])
      expect(availabilities[3].slots).toEqual([])
      expect(availabilities[4].slots).toEqual([])
      expect(availabilities[5].slots).toEqual([])
      expect(availabilities[6].slots).toEqual([])
    })
  })

  describe('simple case', () => {
    beforeEach(async () => {
      await knex('events').insert([
        {
          kind: 'opening',
          starts_at: new Date('2014-08-04 09:30'),
          ends_at: new Date('2014-08-04 12:30'),
          weekly_recurring: true,
        },
        {
          kind: 'appointment',
          starts_at: new Date('2014-08-11 10:30'),
          ends_at: new Date('2014-08-11 11:30'),
        },
      ])
    })

    it('should fetch availabilities correctly', async () => {
      const availabilities = await getAvailabilities(new Date('2014-08-10'))
      expect(availabilities.length).toBe(7)

      expect(String(availabilities[0].date)).toBe(
        String(new Date('2014-08-10')),
      )
      expect(availabilities[0].slots).toEqual([])

      expect(String(availabilities[1].date)).toBe(
        String(new Date('2014-08-11')),
      )
      expect(availabilities[1].slots).toEqual([
        '9:30',
        '10:00',
        '11:30',
        '12:00',
      ])

      expect(String(availabilities[6].date)).toBe(
        String(new Date('2014-08-16')),
      )
    })
  })

  describe('opening only case', () => {
    beforeEach(async () => {
      await knex('events').insert([
        {
          kind: 'opening',
          starts_at: new Date('2014-08-04 09:30'),
          ends_at: new Date('2014-08-04 12:30'),
          weekly_recurring: true,
        },
      ])
    })

    it('should fetch availabilities correctly', async () => {
      const availabilities = await getAvailabilities(new Date('2014-08-18'))
      expect(availabilities.length).toBe(7)

      expect(String(availabilities[0].date)).toBe(
        String(new Date('2014-08-18')),
      )
      expect(availabilities[0].slots).toEqual([
        '9:30',
        '10:00',
        '10:30',
        '11:00',
        '11:30',
        '12:00',
      ])

      expect(String(availabilities[6].date)).toBe(
        String(new Date('2014-08-24')),
      )
    })
  })

  describe('future recurring event case', () => {
    beforeEach(async () => {
      await knex('events').insert([
        {
          kind: 'opening',
          starts_at: new Date('2014-08-25 09:30'),
          ends_at: new Date('2014-08-25 12:30'),
          weekly_recurring: true,
        },
        {
          kind: 'appointment',
          starts_at: new Date('2014-08-11 10:30'),
          ends_at: new Date('2014-08-11 11:30'),
        },
      ])
    })

    it('should not take into account recurring events starting in the future', async () => {
      const availabilities = await getAvailabilities(new Date('2014-08-10'))
      expect(availabilities.length).toBe(7)

      expect(String(availabilities[0].date)).toBe(
        String(new Date('2014-08-10')),
      )

      expect(availabilities[0].slots).toEqual([])
      expect(availabilities[1].slots).toEqual([])
      expect(availabilities[2].slots).toEqual([])
      expect(availabilities[3].slots).toEqual([])
      expect(availabilities[4].slots).toEqual([])
      expect(availabilities[5].slots).toEqual([])
      expect(availabilities[6].slots).toEqual([])

      expect(String(availabilities[6].date)).toBe(
        String(new Date('2014-08-16')),
      )
    })
  })

  describe('too long appointment case', () => {
    beforeEach(async () => {
      await knex('events').insert([
        {
          kind: 'opening',
          starts_at: new Date('2014-08-04 09:30'),
          ends_at: new Date('2014-08-04 12:30'),
          weekly_recurring: true,
        },
        {
          kind: 'appointment',
          starts_at: new Date('2014-08-01 8:00'),
          ends_at: new Date('2014-08-31 23:00'),
        },
        // This person has probably an appointment with its vacations
      ])
    })

    it('should have no slots', async () => {
      const availabilities = await getAvailabilities(new Date('2014-08-10'))
      expect(availabilities.length).toBe(7)

      expect(String(availabilities[0].date)).toBe(
        String(new Date('2014-08-10')),
      )

      expect(availabilities[0].slots).toEqual([])
      expect(availabilities[1].slots).toEqual([])
      expect(availabilities[2].slots).toEqual([])
      expect(availabilities[3].slots).toEqual([])
      expect(availabilities[4].slots).toEqual([])
      expect(availabilities[5].slots).toEqual([])
      expect(availabilities[6].slots).toEqual([])

      expect(String(availabilities[6].date)).toBe(
        String(new Date('2014-08-16')),
      )
    })
  })

  describe('too many appointments case', () => {
    beforeEach(async () => {
      await knex('events').insert([
        {
          kind: 'opening',
          starts_at: new Date('2014-08-04 09:30'),
          ends_at: new Date('2014-08-04 12:30'),
          weekly_recurring: true,
        },
        {
          kind: 'appointment',
          starts_at: new Date('2014-08-11 09:30'),
          ends_at: new Date('2014-08-11 10:00'),
        },
        {
          kind: 'appointment',
          starts_at: new Date('2014-08-11 10:00'),
          ends_at: new Date('2014-08-11 11:00'),
        },
        {
          kind: 'appointment',
          starts_at: new Date('2014-08-11 11:00'),
          ends_at: new Date('2014-08-11 12:30'),
        },
      ])
    })

    it('should have no slots', async () => {
      const availabilities = await getAvailabilities(new Date('2014-08-10'))
      expect(availabilities.length).toBe(7)

      expect(String(availabilities[0].date)).toBe(
        String(new Date('2014-08-10')),
      )

      expect(availabilities[0].slots).toEqual([])
      expect(availabilities[1].slots).toEqual([])
      expect(availabilities[2].slots).toEqual([])
      expect(availabilities[3].slots).toEqual([])
      expect(availabilities[4].slots).toEqual([])
      expect(availabilities[5].slots).toEqual([])
      expect(availabilities[6].slots).toEqual([])

      expect(String(availabilities[6].date)).toBe(
        String(new Date('2014-08-16')),
      )
    })
  })
})
