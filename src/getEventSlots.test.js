import getEventSlots from './getEventSlots'

describe('getEventSlots', () => {
  describe('simple case', () => {
    it('should compute slots correctly', () => {
      const date = new Date('2014-08-03')
      const event = {
        kind: 'opening',
        starts_at: new Date('2014-08-04 09:30'),
        ends_at: new Date('2014-08-04 12:30'),
      }
      const expectedOutput = [
        {index: 1, slot: '9:30'},
        {index: 1, slot: '10:00'},
        {index: 1, slot: '10:30'},
        {index: 1, slot: '11:00'},
        {index: 1, slot: '11:30'},
        {index: 1, slot: '12:00'},
      ]
      
      expect(getEventSlots(date, event)).toEqual(expectedOutput)
    })

    it('should compute indexes correctly', () => {
      const date = new Date('2014-08-01')
      const events = []
      for (let i = 1; i <= 7; ++i) {
        events.push({
          kind: 'opening',
          starts_at: new Date(`2014-08-0${i} 09:30`),
          ends_at: new Date(`2014-08-0${i} 10:00`),
        })
      }
      
      const output = events.map(e => getEventSlots(date, e))
      const expectedOutput = [
        [{index: 0, slot: '9:30'}],
        [{index: 1, slot: '9:30'}],
        [{index: 2, slot: '9:30'}],
        [{index: 3, slot: '9:30'}],
        [{index: 4, slot: '9:30'}],
        [{index: 5, slot: '9:30'}],
        [{index: 6, slot: '9:30'}],
      ]
      
      expect(output).toEqual(expectedOutput)
    })
  })

  describe('recurring event case', () => {
    it('should compute slots correctly', () => {
      const date = new Date('2014-08-18')
      const event = {
        kind: 'opening',
        starts_at: new Date('2014-08-04 09:30'),
        ends_at: new Date('2014-08-04 12:30'),
        weekly_recurring: true,
      }
      const expectedOutput = [
        {index: 0, slot: '9:30'},
        {index: 0, slot: '10:00'},
        {index: 0, slot: '10:30'},
        {index: 0, slot: '11:00'},
        {index: 0, slot: '11:30'},
        {index: 0, slot: '12:00'},
      ]
      
      expect(getEventSlots(date, event)).toEqual(expectedOutput)
    })
  })

  describe('overlapping day case', () => {
    it('should compute slots correctly', () => {
      const date = new Date('2014-08-03')
      const event = {
        kind: 'opening',
        starts_at: new Date('2014-08-04 23:00'),
        ends_at: new Date('2014-08-05 01:30'),
      }
      const expectedOutput = [
        {index: 1, slot: '23:00'},
        {index: 1, slot: '23:30'},
        {index: 2, slot: '0:00'},
        {index: 2, slot: '0:30'},
        {index: 2, slot: '1:00'},
      ]
      
      expect(getEventSlots(date, event)).toEqual(expectedOutput)
    })
  })
})
